﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.UI;


//[NetworkSettings( channel = 0, sendInterval = 0.033f)]
public class CarMovement : NetworkBehaviour {
	
	[SyncVar(hook = "SyncPosVal")] private Vector3 SyncPos;
	[SyncVar(hook = "SyncRotVal")] private Quaternion SyncRot;


	[SerializeField] Transform MyTransform;
	[SerializeField] Transform PlayerTransform;


	public InputField N;
	public InputField FL;
	public InputField C;
	private float LerpRate;
	private float NLerpRate = 10;
	private float FLerpRate = 20;
	private float Closeenough = 0.1f;

	private List<Vector3> SyncPosList = new List<Vector3> ();
	private List<Quaternion> SyncRotList = new List<Quaternion> ();

	[SerializeField] private bool UseHistory = false;

	void Start(){
		N = GameObject.Find ("Normal").GetComponent<InputField> ();
		FL = GameObject.Find ("Fast").GetComponent<InputField> ();
		C = GameObject.Find ("Close").GetComponent<InputField> ();
		NLerpRate = float.Parse(N.text.ToString());
		FLerpRate = float.Parse(FL.text.ToString());
		Closeenough = float.Parse(C.text.ToString());

		LerpRate = NLerpRate;
	}

	// Update is called once per frame
	void Update(){
		LerpPosition ();

	}
	void FixedUpdate () {
		TransmitPos ();
	}


	[Client]
	void SyncPosVal(Vector3 v){
		SyncPos = v;
		SyncPosList.Add (v);
	}

	[Client]
	void SyncRotVal(Quaternion q){
		SyncRot = q;
		SyncRotList.Add (q);
	}

	void LerpPosition(){
		if (!isLocalPlayer) {
			if (UseHistory) {
				HistoricalLerp ();
			}
			else 
			{
				MyTransform.position = Vector3.Lerp (MyTransform.position, SyncPos, Time.deltaTime*LerpRate);
				PlayerTransform.transform.rotation = Quaternion.Lerp (PlayerTransform.transform.rotation, SyncRot, Time.deltaTime*LerpRate);

			}


		}
	}

	[Command]
	void CmdProvidePosToServer(Vector3 pos, Quaternion playerRot){
		SyncPos = pos;
		SyncRot = playerRot;

	}

	[ClientCallback]
	void TransmitPos(){
		if (isLocalPlayer) {
			CmdProvidePosToServer (MyTransform.position, PlayerTransform.rotation);
		}
	}
	void HistoricalLerp()
	{
		if (SyncPosList.Count > 0) {
			MyTransform.position = Vector3.Lerp (MyTransform.position, SyncPosList[0], Time.deltaTime*LerpRate);
			print ("Distance "+Vector3.Distance (MyTransform.position, SyncPosList [0]));
			if (Vector3.Distance (MyTransform.position, SyncPosList [0]) < Closeenough) {
				SyncPosList.RemoveAt (0);
			}
			if (SyncPosList.Count > 5) {
				LerpRate = FLerpRate;
			}
			else
			{
				LerpRate = NLerpRate;
			}
			print ("list " + SyncPosList.Count.ToString());
		}


		if (SyncRotList.Count > 0) {
			PlayerTransform.transform.rotation = Quaternion.Lerp (PlayerTransform.transform.rotation, SyncRotList[0], Time.deltaTime*LerpRate);
			print ("angle "+ Quaternion.Angle(PlayerTransform.transform.rotation, SyncRotList [0]));
			if (Quaternion.Angle (PlayerTransform.transform.rotation, SyncRotList [0]) < Closeenough) {
				SyncRotList.RemoveAt (0);
			}
			if (SyncRotList.Count > 5) {
				LerpRate = FLerpRate;
			}
			else
			{
				LerpRate = NLerpRate;
			}
			print ("list " + SyncRotList.Count.ToString());
		}
	}

}
